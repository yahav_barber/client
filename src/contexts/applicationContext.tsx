import { Appointment } from '../interfaces/appointment';
import { Availability, Barber } from '../interfaces/barber';
import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { barberEnterAvailability, getBarberFutureSchedule, getBarbers, getClientFutureAppointments, scheduleAppointment } from '../services/backend.service';
import { BarberSchedule } from '../interfaces/barberAppointment';
import { User } from '@firebase/auth';
import 'react-notifications/lib/notifications.css';
import { NotificationManager } from 'react-notifications';

interface ApplicationContextType {
    barber: Barber;
    setBarber: Dispatch<SetStateAction<Barber>>;
    userFutureAppointments: Appointment[];
    barberFutureSchedule: BarberSchedule,
    setUserAppointments: Dispatch<SetStateAction<Appointment[]>>;
    userDeleteAppointment: Dispatch<SetStateAction<Appointment>>;
    clientScheduleAppointment: Dispatch<SetStateAction<Appointment>>;
    loadClientFutureAppointments: (clientId: string, idToken?: string) => void;
    onBarberEnterAvailability: Dispatch<SetStateAction<Availability>>;
    setLoggedFirebaseUser: Dispatch<SetStateAction<User>>;
}

const defaultContextValue: ApplicationContextType = {
    barber: null,
    setBarber: () => { },
    userFutureAppointments: [],
    barberFutureSchedule: null,
    setUserAppointments: () => { },
    userDeleteAppointment: () => { },
    clientScheduleAppointment: () => { },
    loadClientFutureAppointments: () => { },
    onBarberEnterAvailability: () => { },
    setLoggedFirebaseUser: () => { }

}

export const ApplicationContext = React.createContext(defaultContextValue);

export const ApplicationProvider: React.FC = ({ children }) => {
    const [barber, setBarber] = useState<Barber>();
    const [userFutureAppointments, setUserAppointments] = useState<Appointment[]>([]);
    const [barberFutureSchedule, setBarberFutureSchedule] = useState<BarberSchedule>({});
    const [loggedFirebaseUser, setLoggedFirebaseUser] = useState<User>();

    const loadBarberDetails = async () => {
        const idToken = await loggedFirebaseUser.getIdToken();
        const barbers = await getBarbers();
        setBarber(barbers[0]);

        const barberFutureSchedule = await getBarberFutureSchedule(barbers[0]._id, idToken)
        setBarberFutureSchedule(barberFutureSchedule);

    }

    const loadClientFutureAppointments = async (clientId: string, idToken?: string) => {
        const appointments = await getClientFutureAppointments(clientId, idToken || (await loggedFirebaseUser.getIdToken()));
        setUserAppointments(appointments)
    }

    const clientScheduleAppointment = async (appointment: Appointment) => {
        const newAppointment = await scheduleAppointment(appointment, await loggedFirebaseUser.getIdToken());
        setUserAppointments([...userFutureAppointments, newAppointment]);
        await loadBarberDetails();

        NotificationManager.success(`קבעת תור בהצלחה ב${appointment.date} ל ${appointment.timeRange.split("-")[0]}`, `!מעולה`);
    }

    const onBarberEnterAvailability = async (availability: Availability) => {
        const barberWithUpdatedAvailablity = await barberEnterAvailability(barber._id, availability,
            await loggedFirebaseUser.getIdToken());
        setBarber(barberWithUpdatedAvailablity);
        NotificationManager.success('הזמינות הוזנה בהצלחה', 'וואלק טיל');
    }

    const userDeleteAppointment = (appointment: Appointment) => {
        const updatedBarberAvailability = { ...barber.availability };
        updatedBarberAvailability[appointment.date].push(appointment.timeRange);
        setBarber({
            ...barber,
            availability: updatedBarberAvailability
        });
        setUserAppointments(userFutureAppointments.filter(futureAppointment =>
            futureAppointment._id !== appointment._id))
    }

    useEffect(() => {
        if (loggedFirebaseUser) {
            loadBarberDetails();
        }
    }, [loggedFirebaseUser])

    return (
        <ApplicationContext.Provider value={{
            barber, setBarber, userFutureAppointments,
            setUserAppointments, userDeleteAppointment,
            clientScheduleAppointment,
            loadClientFutureAppointments,
            onBarberEnterAvailability,
            barberFutureSchedule,
            setLoggedFirebaseUser
        }}>
            {children}
        </ApplicationContext.Provider >
    );
}