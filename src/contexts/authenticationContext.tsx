import React, { Dispatch, SetStateAction, useContext, useEffect, useState } from 'react';
import { getAuth, User, onAuthStateChanged } from '@firebase/auth';
import { initializeApp } from '@firebase/app';
import { firebaseConfig } from '../helpers/firebase';
import { Barber } from '../interfaces/barber';
import { Client } from '../interfaces/client';
import isEmpty from 'lodash/isEmpty';
import { UserType } from '../enums/userType';
import { ApplicationContext } from './applicationContext';
import { UserDetails } from 'interfaces/userDetails';
import { getUserDetailsByPhoneNumber } from '../services/backend.service';

interface AuthenticationContextType {
    loggedUser: User;
    setLoggedUser: Dispatch<SetStateAction<User>>;
    loggedBarber: Barber;
    loggedClient: Client;
    updateLoggedUserDetails: (userDetails: UserDetails, user: User) => void;
    isLoading: boolean;
}

const defaultContextValue: AuthenticationContextType = {
    loggedUser: null,
    setLoggedUser: () => { },
    updateLoggedUserDetails: () => { },
    loggedBarber: null,
    loggedClient: null,
    isLoading: false
}

export const AuthenticationContext = React.createContext(defaultContextValue);

export const AuthenticationProvider: React.FC = ({ children }) => {
    const { loadClientFutureAppointments: loadUserFutureAppointments, setLoggedFirebaseUser } = useContext(ApplicationContext);
    const [loggedUser, setLoggedUser] = useState<User>();
    const [loggedBarber, setLoggedBarber] = useState<Barber>();
    const [loggedClient, setLoggedClient] = useState<Client>();
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        initializeApp(firebaseConfig);
        const auth = getAuth();
        onAuthStateChanged(auth, user => {
            if (user) {
                setLoggedUser(user);
                setLoggedFirebaseUser(user);
                handleLoggedUser(user);
            } else {
                setIsLoading(false)
            }
        });
    }, []);

    useEffect(() => {
        if (!isEmpty(loggedBarber) || !isEmpty(loggedClient)) {
            setIsLoading(false)
        }
    }, [loggedBarber, loggedClient])

    const handleLoggedUser = async (firebaseUser: User) => {
        const userDetails = await getUserDetailsByPhoneNumber(firebaseUser.phoneNumber);
        updateLoggedUserDetails(userDetails, firebaseUser);
    }

    const updateLoggedUserDetails = async (userDetails: UserDetails, user: User) => {
        if (!isEmpty(userDetails)) {
            if (userDetails.userType === UserType.BARBER) {
                setLoggedBarber(userDetails.user as Barber);
            } else if (userDetails.userType === UserType.CLIENT) {
                const clientDetails = userDetails.user as Client;

                loadUserFutureAppointments(clientDetails._id, await user.getIdToken());
                setLoggedClient(clientDetails);
            }
        }
        setIsLoading(false)
    }

    return (
        <AuthenticationContext.Provider value={{
            loggedUser, setLoggedUser,
            updateLoggedUserDetails, loggedBarber, loggedClient,
            isLoading
        }}>
            {children}
        </AuthenticationContext.Provider >
    );
}