import axios from 'axios'
import { UserDetails } from 'interfaces/userDetails';
import { UserType } from '../enums/userType';
import { Appointment } from '../interfaces/appointment';
import { Availability, Barber } from '../interfaces/barber';
import { BarberSchedule } from '../interfaces/barberAppointment';
import { Client } from '../interfaces/client';

export async function getBarbers() {
    const barbers = await axios.get<Barber[]>(`${process.env.GATSBY_BACKEND_URL}/barbers`);

    return barbers.data;
}

export async function getClientFutureAppointments(userId: string, idToken: string) {
    const barbers = await axios.get<Appointment[]>(`${process.env.GATSBY_BACKEND_URL}/appointments/future?userId=${userId}`, {
        headers: {
            Authorization: `Bearer ${idToken}`
        }
    });

    return barbers.data;
}

export async function getBarberFutureSchedule(barberId: string, idToken: string) {
    const schedule = await axios.get<BarberSchedule>(`${process.env.GATSBY_BACKEND_URL}/barbers/${barberId}/futureSchedule`, {
        headers: {
            Authorization: `Bearer ${idToken}`
        }
    });

    return schedule.data;
}

export async function deleteAppointmentById(appointmentId: string, idToken: string) {
    const appointment = await axios.delete<Appointment>(`${process.env.GATSBY_BACKEND_URL}/appointments/${appointmentId}`, {
        headers: {
            Authorization: `Bearer ${idToken}`
        }
    });

    return appointment.data;
}

export async function scheduleAppointment(appointment: Appointment, idToken: string) {
    const appointmentResponse = await axios.post<Appointment>(`${process.env.GATSBY_BACKEND_URL}/appointments`, appointment, {
        headers: {
            Authorization: `Bearer ${idToken}`
        }
    });

    return appointmentResponse.data;
}

export async function barberEnterAvailability(barberId: string, availability: Availability, idToken: string) {
    const response = await axios.post<Barber>(`${process.env.GATSBY_BACKEND_URL}/barbers/${barberId}/availability`, availability, {
        headers: {
            Authorization: `Bearer ${idToken}`
        }
    });

    return response.data;
}

export async function getUserDetailsByPhoneNumber(phoneNumber: string): Promise<UserDetails> {
    try {
        const userDetailsResponse = await axios.get<UserDetails>(`${process.env.GATSBY_BACKEND_URL}/users?phoneNumber=${encodeURIComponent(phoneNumber)}`);

        return userDetailsResponse.data;
    }
    catch (err) {
        console.error(err);
    }
}

export async function addNewClient(phoneNumber: string, name: string, idToken: string): Promise<UserDetails> {
    try {
        const appointmentResponse = await axios.post<Client>(`${process.env.GATSBY_BACKEND_URL}/clients`, {
            name,
            phoneNumber,
        },
            {
                headers: {
                    Authorization: `Bearer ${idToken}`
                }
            });

        return {
            userType: UserType.CLIENT,
            user: appointmentResponse.data
        }
    }
    catch (err) {
        return {} as UserDetails;
    }
}