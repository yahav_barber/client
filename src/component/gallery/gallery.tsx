import React, { useEffect, useState } from "react";
import './gallery.scss'
import { graphql, useStaticQuery } from "gatsby";


const Gallery = () => {
  const [imageIndex, setImageIndex] = useState(0);
  const { allFile } = useStaticQuery(graphql`
        query {
            allFile(filter: {
              relativeDirectory: {eq: "haircuts"}}) 
            {
              edges {
                node {
                  childImageSharp {
                    fluid(maxWidth: 500) {
                        originalName
                        src
                    }
                  }
                  
                }
              }
            }
          }`);

  useEffect(() => {
    const interval = setInterval(() => {
      setImageIndex(index => (index + 1 + allFile?.edges.length) % allFile?.edges.length)
    }, 3000)

    return () => clearInterval(interval);

  }, [allFile])


  return (
    <div className="background-image-wrapper">
      <img className="background-image" src={allFile?.edges[imageIndex].node.childImageSharp.fluid.src} />
    </div>
  );
};

export default Gallery;
