import { Appointment } from "interfaces/appointment";
import React from "react";
import "./userAppointment.scss";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

interface UserAppointmentProps {
  appointment: Appointment;
  deleteAppointment: (appointment: Appointment) => void;
}

const UserAppointment = ({ appointment, deleteAppointment }: UserAppointmentProps) => {

  return (
    <div className="appointment">
      <div className="date">
        {appointment.date.replace(/-/g, '/')}
      </div>
      <div className="time">
        {appointment.timeRange}
      </div>
      <FontAwesomeIcon icon={faTrash} className="trash-icon" onClick={() => deleteAppointment(appointment)} />
    </div>
  )
};

export default UserAppointment;
