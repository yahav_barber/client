import React, { useContext, useState } from "react";
import classnames from "classnames";
import "./register.scss";
import Button from "../button/button";
import { getAuth, RecaptchaVerifier, signInWithPhoneNumber, } from "firebase/auth";
import { ISRAELI_PHONE_NUMBER_REGEX } from "../../helpers/constants";
import isEmpty from 'lodash/isEmpty';
import Loader from "react-loader-spinner";
import { AuthenticationContext } from "../../contexts/authenticationContext";
import { addNewClient, getUserDetailsByPhoneNumber } from "../../services/backend.service";
import { UserDetails } from "interfaces/userDetails";


const Register = () => {
  const { setLoggedUser, updateLoggedUserDetails } = useContext(AuthenticationContext)
  const [doesSMSsent, setDoesSMSsent] = useState<boolean>(false);
  const [enteredPhoneNumber, setEnteredPhoneNumber] = useState<string>("");
  const [formattedPhoneNumber, setFormattedPhoneNumber] = useState<string>();
  const [verificationCode, setVerificationCode] = useState<string>("");
  const [isLoadingUserDetails, setIsLoadingUserDetails] = useState<boolean>();
  const [userDetails, setUserDetails] = useState<UserDetails>();
  const [userName, setUserName] = useState<string>('');
  const [doesRegisterFailed, setDoesRegisterFailed] = useState<boolean>(false);

  const onChangePhoneNumber = (phoneNumber: string) => {
    const userPhoneNumber = `+${phoneNumber}`;
    let formattedPhoneNumber = '';
    if (ISRAELI_PHONE_NUMBER_REGEX.test(userPhoneNumber)) {
      formattedPhoneNumber = `+972${phoneNumber.substring(1)}`;
    }
    setEnteredPhoneNumber(phoneNumber);
    setFormattedPhoneNumber(formattedPhoneNumber)
    setIsLoadingUserDetails(false)
    setDoesSMSsent(false)
    setUserDetails(undefined)
  }

  const setUpRecaptcha = () => {
    const auth = getAuth();
    window.recaptchaVerifier = new RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
        callback: function () {
          console.log("Captcha Resolved");
        },
        defaultCountry: "IN",
      },
      auth
    );
  };

  const sendConfirmToPhone = () => {
    const auth = getAuth();
    setUpRecaptcha();
    setIsLoadingUserDetails(true)

    signInWithPhoneNumber(auth, formattedPhoneNumber, window.recaptchaVerifier)
      .then(async function (confirmationResult) {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        setDoesSMSsent(true);
        window.confirmationResult = confirmationResult;

        console.log("OTP is sent", confirmationResult);

        const userDetails = await getUserDetailsByPhoneNumber(formattedPhoneNumber);

        setUserDetails(userDetails);
        setIsLoadingUserDetails(false)
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const clickOnSignIn = () => {
    window.confirmationResult.confirm(verificationCode).then(async (result) => {
      console.log("result", result);
      setLoggedUser(result.user);
      if (userDetails) {
        updateLoggedUserDetails(userDetails, result.user);
      } else {
        const newClient = await addNewClient(formattedPhoneNumber, userName, await result.user.getIdToken());
        updateLoggedUserDetails(newClient, result.user)
      }
      // ...
    }).catch((err) => {
      setDoesRegisterFailed(true);
    });
  }

  return (
    <div className="register-container">
      <div className="input-container">
        <div className="message">הכנס מספר טלפון</div>
        <div className="phone-input-container">
          <div className="input">
            <input type="tel"
              value={enteredPhoneNumber}
              className={classnames("text-input", "ltr")}
              onChange={e => onChangePhoneNumber(e.target.value)} />
          </div>
          <div className="send-message-button-container">
            <Button
              disable={!formattedPhoneNumber}
              customClass={"send-message-button"}
              onClick={sendConfirmToPhone}>
              שלח אימות
            </Button>
          </div>
        </div>
      </div>
      {doesSMSsent && !isLoadingUserDetails ? <div className="input-container">
        <div className="message">הכנס שם</div>
        <input type="input" className="text-input" disabled={!isEmpty(userDetails)}
          value={isEmpty(userDetails) ? userName : userDetails.user.name} onChange={e => setUserName(e.target.value)} />
      </div> : <div />}
      <div id="recaptcha-container"></div>
      {isLoadingUserDetails ? <div className="loader-wrapper">
        <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
      </div> : <div />}
      {doesSMSsent && !isLoadingUserDetails ? <div className="input-container">
        <div className="message">הכנס קוד אימות</div>
        <input type="input"
          value={verificationCode}
          className={classnames("text-input", { error: doesRegisterFailed })}
          onChange={e => setVerificationCode(e.target.value)} />
      </div> : <div />}
      <div className="register-button-container">
        <Button
          disable={!doesSMSsent || !verificationCode || isLoadingUserDetails}
          customClass={"register-button"}
          onClick={clickOnSignIn}>
          היכנס
        </Button>
      </div>
    </div >
  );
};

export default Register;
