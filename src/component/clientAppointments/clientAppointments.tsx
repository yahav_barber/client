import classnames from "classnames";
import UserAppointment from "../userAppointment/userAppointment";
import { ApplicationContext } from "../../contexts/applicationContext";
import React, { useContext, useRef, useState } from "react";
import { useOnClickOutside } from "../../helpers/clickOutside";
import "./clientAppointments.scss";
import { Appointment } from "interfaces/appointment";
import { deleteAppointmentById } from "../../services/backend.service";

interface ClientAppointmentsProps {
  show: boolean;
  dismiss: () => void;
}

const ClientAppointments = ({ show, dismiss }: ClientAppointmentsProps) => {
  const ref = useRef<HTMLDivElement>(null);
  const { userFutureAppointments, userDeleteAppointment } = useContext(ApplicationContext)

  useOnClickOutside([ref], () => {
    dismiss();
  });

  const deleteAppointment = async (appointment: Appointment) => {
    await deleteAppointmentById(appointment._id);
    userDeleteAppointment(appointment)
  }

  return (
    <div
      ref={ref}
      className={classnames("client-appointments-container", { show: show })}>
      {
        userFutureAppointments.map(appointment => {
          return (
            <UserAppointment
              key={appointment._id}
              appointment={appointment}
              deleteAppointment={deleteAppointment} />
          )
        })
      }
    </div>
  );
};

export default ClientAppointments;
