import { AuthenticationContext } from "../../contexts/authenticationContext";
import React, { useContext } from "react";
import "./header.scss";
import instagramIcon from '../../images/instagramIcon.png'

interface HeaderProps {
}

const Header = ({ }: HeaderProps) => {
    const { loggedBarber, loggedClient } = useContext(AuthenticationContext);

    const onClickInstagramButton = () => {
        window.open("https://instagram.com/yahavs__barber")
    }

    return (
        <div className="application-header">
            <div className="first-side">
                <div className='header-logo'>
                    YahavS
                </div>
                <div className="instagram-icon-wrapper" onClick={() => onClickInstagramButton()}>
                    <img src={instagramIcon} />
                </div>
            </div>
            <div className='header-user'>ברוך הבא {loggedBarber ? loggedBarber.name : loggedClient ? loggedClient.name : 'אורח'}</div>
        </div>
    )
};

export default Header;