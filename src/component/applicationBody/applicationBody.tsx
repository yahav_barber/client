import { AuthenticationContext } from "../../contexts/authenticationContext";
import React, { useContext, useState } from "react";
import "./applicationBody.scss";
import Register from "../register/register";
import Button from "../button/button";
import ClientAppointments from "../clientAppointments/clientAppointments";
import OrderForms from "../orderForms/orderForms";
import Loader from "react-loader-spinner";
import { BarberSchedule } from "../barberSchedule/barberSchedule";
import classNames from 'classnames'

interface ApplicationBodyProps {
    showForm: boolean;
    setShowForm: (showForm: boolean) => void;
}

const ApplicationBody = ({ showForm, setShowForm }: ApplicationBodyProps) => {
    const [showAppointments, setShowAppointments] = useState(false);
    const { loggedClient, loggedBarber, isLoading } = useContext(AuthenticationContext);
    const [showSchedule, setShowSchedule] = useState(false);

    return (
        <div className="application-body">
            {isLoading ? <div className="loader">
                <Loader type="ThreeDots" color="#ffffff" height={70} width={70} />
            </div> :
                !loggedClient && !loggedBarber ? <div className="register-wrapper">
                    <Register />
                </div>
                    : <div className="content-container">
                        <div className="title-container">
                            <h1>Yahav's Hair Design </h1>
                        </div>
                        <div className="sub-title-container">
                            <h2>Here You Can Book or Reschedule An Appointment</h2>
                        </div>
                        <div className="button-container">
                            <Button
                                onClick={() => {
                                    setShowForm(!showForm);
                                }}
                            >
                                {loggedClient ? 'הזמן תספורת' : 'הזן לוז'}
                            </Button>
                        </div>
                        <OrderForms
                            isClientLogin={!!loggedClient}
                            show={showForm}
                            dismiss={() => {
                                setShowForm(false);
                            }}
                        />
                    </div>}
            {showSchedule && <BarberSchedule dismiss={() => {
                setShowSchedule(show => !show)
            }} />}
            <div className={classNames("side-wrapper", { show: !showForm })}>
                {<div className={classNames("button-container")}>
                    {loggedClient && <Button
                        customClass={"my-appointments-button"}
                        onClick={() => {
                            setShowAppointments((show) => !show);
                        }}
                    >
                        התורים שלי
                    </Button>}
                    {loggedBarber && <div className="button-container">
                        <Button
                            customClass={"my-schedule-button"}
                            onClick={() => {
                                setShowSchedule((show) => !show);
                            }}
                        >
                            הלוז שלי
                        </Button>
                    </div>}
                    <ClientAppointments
                        show={showAppointments}
                        dismiss={() => {
                            setShowAppointments(false);
                        }} />
                </div>}
            </div>
        </div>
    )
};

export default ApplicationBody;
