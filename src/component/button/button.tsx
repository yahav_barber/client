import React from "react";
import classnames from "classnames";
import Loader from "react-loader-spinner";

import "./button.scss";

const Button = ({
  onClick,
  children,
  disable = false,
  loading = false,
  customClass = "",
}: {
  onClick: () => any;
  children: any;
  disable?: boolean;
  loading?: boolean;
  customClass?: string;
}) => {
  return (
    <div
      onClick={async () => {
        if (!disable) {
          await onClick();
        }
      }}
      className={classnames("button", customClass, { disable })}
    >
      {loading ? (
        <Loader type="TailSpin" color="#ffffff" height={30} width={30} />
      ) : (
        children
      )}
    </div>
  );
};

export default Button;
