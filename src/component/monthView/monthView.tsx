import React, { useEffect, useState } from "react";
import Next from "../../assets/IconArrowLeft.svg";
import Back from "../../assets/IconArrowRight.svg";
import "./monthView.scss";
import moment from 'moment'
import classNames from "classnames";
import { NotificationContainer, NotificationManager } from 'react-notifications';

const DAYS = [
  "רא",
  "שנ",
  "של",
  "רב",
  "חמ",
  "שי",
  "שב"
];

const Mouths = [
  "ינואר",
  "פברואר",
  "מרץ",
  "אפריל",
  "מאי",
  "יוני",
  "יולי",
  "אוגוסט",
  "ספטמבר",
  "אוקטובר",
  "נובמבר",
  "דצמבר",
];

interface WeekDay {
  momentDate: moment.Moment;
  doesBarberAvailableThatDay: boolean;
  previousDay: boolean;
}

interface MonthViewProps {
  availableDays: string[];
  onChooseDate: (moment: moment.Moment) => void;
  isClientLogin: boolean;
}


const MonthView = ({ availableDays, onChooseDate, isClientLogin }: MonthViewProps) => {
  const [weekDays, setWeekDays] = useState<WeekDay[]>([]);
  const [choosesDate, setChoosesDate] = useState<moment.Moment>();
  const [weekNumber, setWeekNumber] = useState<number>(moment().week())

  const onDateClicked = (weekDayIndex: number) => {
    const weekDay = weekDays[weekDayIndex];

    if (weekDay.momentDate.isSameOrAfter(moment(), 'day') && (!isClientLogin || weekDay.doesBarberAvailableThatDay)) {
      setChoosesDate(weekDay.momentDate);
      onChooseDate(weekDay.momentDate)
    }

    if (isClientLogin && !weekDay.doesBarberAvailableThatDay) {
      NotificationManager.error('כרגע אני לא עובד ביום הזה', 'לא ניתן לקבוע תור בתאריך זה', 1500);
    }
  };

  useEffect(() => {
    const weekStart = moment().week(weekNumber).startOf('week');
    const days: WeekDay[] = [];

    for (let i = 0; i <= 6; i++) {
      const weekDate = moment(weekStart).add(i, 'day');
      const formattedDate = weekDate.format("DD-MM-YY");
      const doesDatePreviousToToday = weekDate.isBefore(moment(), "day");

      days.push({
        momentDate: weekDate,
        doesBarberAvailableThatDay: availableDays.includes(formattedDate) && !doesDatePreviousToToday,
        previousDay: doesDatePreviousToToday
      });
    }

    setWeekDays(days)
  }, [weekNumber, availableDays])

  const week = moment().week(weekNumber);

  return (
    <div className="schedule-calender-month">
      <div className="months-part">
        <div onClick={() => { setWeekNumber(weekNumber + 1) }} className={"schedule-calender-month-button"}>
          <Next />
        </div>
        <div className={"text"}>{`${Mouths[week.month()]} ${week.year()}`}</div>
        <div onClick={() => { setWeekNumber(weekNumber - 1) }} className={"schedule-calender-month-button"}>
          <Back />
        </div>
      </div>
      <div className="days-part">
        {
          weekDays.map((weekDay, index) => {
            return (<div className="day" key={index}>
              <div className="week-day">
                {DAYS[index]}
              </div>
              <div onClick={() => onDateClicked(index)}
                className={classNames("week-date", {
                  optional: !isClientLogin && !weekDay.previousDay,
                  available: weekDay.doesBarberAvailableThatDay,
                  "previous-day": weekDay.previousDay,
                  active: weekDay.momentDate.toDate().getTime() === choosesDate?.toDate().getTime()
                })}>
                {weekDay.momentDate.date()}
              </div>
            </div>)
          })
        }
      </div>
    </div >
  );
};

export default MonthView;
