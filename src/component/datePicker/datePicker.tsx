import React, { useEffect, useState } from "react";
import "./datePicker.scss";
import MonthView from "../monthView/monthView";
import { Moment } from "moment";
import TimeView from "../timeView/timeVIew";
import {
  formatDateToTime, getDatesOptionsBetweenTimeRanges, getFinishTimeOfAppointmentDate,
  isTimeRangeIntersectsWithOtherTimeRange, isTimeRangeConflictWithBarberAppointments
} from "../../helpers/dates";
import { Appointment } from "../../interfaces/appointment";
import { Barber } from "../../interfaces/barber";
import { Client } from "../../interfaces/client";
import { BarberSchedule } from "../../interfaces/barberAppointment";
import isEmpty from 'lodash/isEmpty';
import TimeRangePicker from "../TimeRangePicker/timeRangePicker";
import { TimeRange } from "../../interfaces/timeRange";


interface DatePickerProps {
  onClientChooseAppointment: (appointment: Appointment) => void,
  barber: Barber;
  loggedClient: Client;
  isClientLogin: boolean;
  barberFutureSchedule: BarberSchedule;
  enteredBarberSchedule: { [key: string]: TimeRange[] };
  onBarberChooseTimeRange: (date: string, timeRanges: TimeRange[]) => void;
}

const DatePicker = ({ onClientChooseAppointment, barber, loggedClient, enteredBarberSchedule,
  isClientLogin, barberFutureSchedule, onBarberChooseTimeRange }: DatePickerProps) => {
  const [availableDays, setAvailableDays] = useState<string[]>([]);
  const [timeOptions, setTimeOptions] = useState<Date[]>([]);
  const [choosesDate, setChosenDate] = useState<string>();
  const [barberAvailableTimeRangesInDate, setBarberAvailableTimeRangesInDate] = useState<TimeRange[]>([]);

  useEffect(() => {
    let timeOptions = [];
    if (barber && choosesDate) {
      updateBarberAvailableTimeRangesInDate();
      if (isClientLogin) {
        timeOptions = getDatesOptionsBetweenTimeRanges(barber.availability[choosesDate], barber.appointmentMinutes);
      } else if (barberFutureSchedule && barberFutureSchedule[choosesDate]) {
        timeOptions = getDatesOptionsBetweenTimeRanges(barberFutureSchedule[choosesDate].map(schedule => schedule.timeRange));
      }
    }

    if (!isEmpty(timeOptions)) {
      setTimeOptions(timeOptions)
    } else {
      setTimeOptions([]);
    }
  }, [barberFutureSchedule, choosesDate, barber]);

  const updateBarberAvailableTimeRangesInDate = () => {
    if (enteredBarberSchedule[choosesDate]) {
      setBarberAvailableTimeRangesInDate(enteredBarberSchedule[choosesDate]);
    } else if (barber.availability[choosesDate]) {
      setBarberAvailableTimeRangesInDate(barber.availability[choosesDate].map(timeRange => {
        return {
          timeRange,
          isPossible: true
        }
      }));
    } else {
      setBarberAvailableTimeRangesInDate([]);
    }
  }

  const onChooseDate = (momentDate: Moment) => {
    setChosenDate(momentDate.format("DD-MM-YY"))
  }

  const onChooseStartTime = (startTime: Date) => {
    const appointmentStartTime = formatDateToTime(startTime);
    const appointmentEndTime = getFinishTimeOfAppointmentDate(startTime, barber.appointmentMinutes);

    if (isClientLogin) {
      onClientChooseAppointment({
        barberId: barber._id,
        clientId: loggedClient._id,
        date: choosesDate,
        timeRange: `${appointmentStartTime}-${appointmentEndTime}`
      })
    }
  }

  const onChangeTimeRange = (timeRanges: TimeRange[]) => {
    const barberTimeRangesInDate: TimeRange[] = [];
    for (let timeRangeIndex = 0; timeRangeIndex < timeRanges.length; timeRangeIndex++) {
      const isConflict = isTimeRangeConflictWithBarberAppointments(timeRanges[timeRangeIndex].timeRange, barberFutureSchedule[choosesDate]);
      const isIntersect = isTimeRangeIntersectsWithOtherTimeRange(timeRanges.map(timeRange => timeRange.timeRange), timeRangeIndex);
      barberTimeRangesInDate.push({ timeRange: timeRanges[timeRangeIndex].timeRange, isPossible: !isConflict && !isIntersect });
    }

    setBarberAvailableTimeRangesInDate(barberTimeRangesInDate);
    onBarberChooseTimeRange(choosesDate, barberTimeRangesInDate);
  }

  useEffect(() => {
    if (barber) {
      setAvailableDays(Object.keys(barber.availability))
    }
  }, [barber])

  return (
    <div className="date-picker">
      <div className="label">בחר יום</div>
      <div className="date-picker-container">
        <MonthView availableDays={availableDays} onChooseDate={onChooseDate} isClientLogin={isClientLogin} />
      </div>
      {choosesDate &&
        <div className="label">{isClientLogin ? "בחר זמן" : "תורים שהוזמנו באותו תאריך"}</div>}
      {choosesDate &&
        <TimeView timeRanges={timeOptions}
          canChooseMultiple={false}
          disabledOptions={!isClientLogin}
          onChooseStartTime={onChooseStartTime} />
      }
      {choosesDate && !isClientLogin && <TimeRangePicker timeRanges={barberAvailableTimeRangesInDate} onChangeTimes={onChangeTimeRange} />}
    </div>
  );
};

export default DatePicker;
