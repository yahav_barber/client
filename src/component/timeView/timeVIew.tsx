import { formatDateToTime } from "../../helpers/dates";
import React, { useEffect, useState } from "react";
import "./timeView.scss";
import classNames from "classnames";

interface TimeViewProps {
  timeRanges: Date[];
  onChooseStartTime: (startTime: Date) => void;
  canChooseMultiple: boolean;
  disabledOptions: boolean;
}

const TimeView = ({ timeRanges, onChooseStartTime, canChooseMultiple, disabledOptions }: TimeViewProps) => {
  const [options, setOptions] = useState<Date[]>([]);
  const [displayedTimeOptions, setDisplayedTimeOptions] = useState<string[]>([]);
  const [choosesOptionsIndexes, setChoosesOptionsIndexes] = useState<number[]>([]);

  useEffect(() => {
    setOptions(timeRanges);
    setDisplayedTimeOptions(timeRanges.map(timeOption => formatDateToTime(timeOption)));
    setChoosesOptionsIndexes([])

  }, [timeRanges]);

  const onChooseTime = (optionIndex: number) => {
    if (!disabledOptions) {
      if (canChooseMultiple) {
        const optionsWithoutOptionIndex = choosesOptionsIndexes.filter(option => option !== optionIndex)
        if (optionsWithoutOptionIndex.length === choosesOptionsIndexes.length) {
          setChoosesOptionsIndexes([...choosesOptionsIndexes, optionIndex]);
        } else {
          setChoosesOptionsIndexes(optionsWithoutOptionIndex)
        }
      } else {
        setChoosesOptionsIndexes([optionIndex]);
      }
      onChooseStartTime(options[optionIndex]);
    }
  }

  return (
    <div className="time-view-container">
      {
        displayedTimeOptions.map((option, index) => {
          return <div key={index}
            className={classNames("option", { active: choosesOptionsIndexes.includes(index), disabled: disabledOptions })}
            onClick={() => onChooseTime(index)}>
            {option}
          </div>
        })
      }
    </div>
  );
};

export default TimeView;

