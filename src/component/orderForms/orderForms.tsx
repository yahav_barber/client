import classnames from "classnames";
import { ApplicationContext } from "../../contexts/applicationContext";
import { Appointment } from "../../interfaces/appointment";
import React, { useContext, useRef, useState } from "react";
import { useOnClickOutside } from "../../helpers/clickOutside";
import Button from "../button/button";
import DatePicker from "../datePicker/datePicker";
import "./orderForms.scss";
import { AuthenticationContext } from "../../contexts/authenticationContext";
import { TimeRange } from "interfaces/timeRange";

const OrderForms = ({
  show,
  dismiss,
  isClientLogin
}: {
  show: boolean;
  dismiss: () => void;
  isClientLogin: boolean
}) => {
  const { barber, clientScheduleAppointment, barberFutureSchedule, onBarberEnterAvailability } = useContext(ApplicationContext);
  const { loggedClient } = useContext(AuthenticationContext);
  const ref = useRef<HTMLDivElement>(null);
  const [choosesAppointment, setChoosesAppointment] = useState<Appointment>();
  const [canSave, setCanSave] = useState<boolean>(false);
  const [enteredBarberSchedule, setEnteredBarberSchedule] = useState<{ [key: string]: TimeRange[] }>({});

  useOnClickOutside([ref], () => {
    dismiss();
  });

  const onClickSaveButton = async () => {
    if (isClientLogin) {
      await clientScheduleAppointment(choosesAppointment);
      dismiss()
    } else {
      const parsedAvailablity = {};
      for (const date in enteredBarberSchedule) {
        parsedAvailablity[date] = enteredBarberSchedule[date].map(timeRange => timeRange.timeRange);
      }
      onBarberEnterAvailability(parsedAvailablity)
    }
  }

  const userChoosesAppointment = (appointment: Appointment) => {
    setChoosesAppointment(appointment);
    setCanSave(true);
  }

  const onBarberChooseTimeRange = (date: string, timeRanges: TimeRange[]) => {
    // const updatedEnteredBarberSchedule = { ...enteredBarberSchedule };
    const updatedEnteredBarberSchedule = {};
    updatedEnteredBarberSchedule[date] = timeRanges;

    setEnteredBarberSchedule(updatedEnteredBarberSchedule);
    let isAllPossible = true;
    for (const date in updatedEnteredBarberSchedule) {
      isAllPossible = isAllPossible && updatedEnteredBarberSchedule[date].every(timeRanges => timeRanges.isPossible);
    }
    if (isAllPossible) {
      setCanSave(true);
    } else {
      setCanSave(false);
    }
  }

  return (
    <div
      ref={ref}
      className={classnames("scroll-up-container", { show: show })}
    >
      <div className="form-title">
        <h1>{isClientLogin ? "קביעת תורים" : "הזנת זמינות"}</h1>
      </div>
      <div className="form-content">
        <DatePicker
          onClientChooseAppointment={userChoosesAppointment}
          barber={barber}
          loggedClient={loggedClient}
          isClientLogin={isClientLogin}
          barberFutureSchedule={barberFutureSchedule}
          enteredBarberSchedule={enteredBarberSchedule}
          onBarberChooseTimeRange={onBarberChooseTimeRange}
        />
      </div>
      <div className="form-submit">
        <Button disable={!canSave} onClick={onClickSaveButton}>{isClientLogin ? "הזמן תור" : "הזן זמינות בתאריך זה"}</Button>
      </div>
    </div>
  );
};

export default OrderForms;
