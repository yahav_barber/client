
import React, { useEffect, useState } from "react";
import "./timeRangePicker.scss";
import classNames from "classnames";
import DateTime from "react-datetime";
import isEmpty from 'lodash/isEmpty';
import { TimeRange } from "../../interfaces/timeRange";
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { formatDateToTime } from "../../helpers/dates";


interface TimePickerProps {
  timeRanges: TimeRange[];
  onChangeTimes: (timeRanges: TimeRange[]) => void;
}

const TimeRangePicker = ({ timeRanges, onChangeTimes }: TimePickerProps) => {
  const [insertedTimeRanges, setInsertedTimeRanges] = useState<TimeRange[]>([]);

  useEffect(() => {
    if (!isEmpty(timeRanges)) {
      setInsertedTimeRanges(timeRanges);
    } else {
      const initialTimeRanges = [{ timeRange: "09:00-18:00", isPossible: true }];
      setInsertedTimeRanges(initialTimeRanges)
      onChangeTimes(initialTimeRanges);
    }
  }, [timeRanges]);

  const onChange = (timeRangeIndex: number, timeInMilliseconds: string | number, fromOrTo: string) => {
    const newTimeRanges = [...insertedTimeRanges];
    const newTime = formatDateToTime(new Date(timeInMilliseconds));

    let [from, to] = newTimeRanges[timeRangeIndex].timeRange.split("-");

    if (fromOrTo === "from") {
      from = newTime;
    } else {
      to = newTime;
    }

    newTimeRanges[timeRangeIndex].timeRange = `${from}-${to}`;

    setInsertedTimeRanges(newTimeRanges);

    if (newTimeRanges[timeRangeIndex].timeRange !== "00:00-23:59") {
      onChangeTimes(newTimeRanges);
    }
  }

  const deleteTimeRange = (timeRangeIndex: number) => {
    const updatedTimeRanges = [...insertedTimeRanges];
    updatedTimeRanges.splice(timeRangeIndex, 1);
    setInsertedTimeRanges(updatedTimeRanges);
    onChangeTimes(updatedTimeRanges);
  }

  const addTimeRange = () => {
    const updatedTimeRanges = [...insertedTimeRanges];
    updatedTimeRanges.push({ timeRange: "00:00-23:59", isPossible: false });
    setInsertedTimeRanges(updatedTimeRanges);
    onChangeTimes(updatedTimeRanges);
  }

  return (
    <div className="time-picker-container">
      <div className="time-picker-messages">
        <label>הזמינות שלי באותו היום</label>
        <div className="add-time-range" onClick={() => addTimeRange()}>טווח זמנים חדש</div>
      </div>
      <div className="time-picker-options">
        {insertedTimeRanges.map((timeRange, index) => {
          const [from, to] = timeRange.timeRange.split("-");
          return (
            <div key={index} className={"time-picker-option-container"}>
              <div className={classNames("red-asterisk", { show: !timeRange.isPossible })}>*</div>
              <div className="time-picker-option">
                <DateTime
                  value={from}
                  input={false}
                  onChange={(value) => onChange(index, value.valueOf(), "from")}
                  dateFormat={false}
                  className="date-time"
                  timeFormat="HH:mm"
                  timeConstraints={{
                    hours: { min: 0, max: 23, step: 1 },
                    minutes: { min: 0, max: 59, step: 1 },
                    seconds: { min: 0, max: 59, step: 1 }
                  }}
                />
                <div className="hyphen">-</div>
                <DateTime
                  value={to}
                  input={false}
                  onChange={(value) => onChange(index, value.valueOf(), "to")}
                  dateFormat={false}
                  className="date-time"
                  timeFormat="HH:mm"
                  timeConstraints={{
                    hours: { min: 0, max: 23, step: 1 },
                    minutes: { min: 0, max: 59, step: 1 },
                    seconds: { min: 0, max: 59, step: 1 }
                  }}
                />
              </div>
              <FontAwesomeIcon icon={faTrash} className="delete-time-range" onClick={() => deleteTimeRange(index)} />
            </div>)
        })}
      </div>
    </div>
  );
};

export default TimeRangePicker;