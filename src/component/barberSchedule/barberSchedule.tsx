import * as React from 'react';
import { ViewState, AppointmentModel } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  WeekView,
  Toolbar,
  DateNavigator,
  Appointments,
  TodayButton,
  AppointmentTooltip
} from '@devexpress/dx-react-scheduler-material-ui';
import './barberSchedule.scss';
import { ApplicationContext } from '../../contexts/applicationContext';
import { useContext, useEffect, useRef, useState } from 'react';
import { convertTimeRangeInDateToDateObjects } from '../../helpers/dates';
import { useOnClickOutside } from '../../helpers/clickOutside';

interface BarberScheduleProps {
  dismiss: () => void;
}

export const BarberSchedule = ({ dismiss }: BarberScheduleProps) => {
  const ref = useRef<HTMLDivElement>(null);
  const { barberFutureSchedule } = useContext(ApplicationContext);
  const [scheduleData, setScheduleData] = useState<AppointmentModel[]>([]);
  const [filteredDate, setFilteredDate] = useState<Date>(new Date);

  useOnClickOutside([ref], () => {
    dismiss();
  });

  useEffect(() => {
    const dates: AppointmentModel[] = [];
    for (const date in barberFutureSchedule) {
      for (const futureSchedule of barberFutureSchedule[date]) {
        const [startDate, endDate] = convertTimeRangeInDateToDateObjects(date, futureSchedule.timeRange)
        dates.push({
          startDate,
          endDate,
          id: futureSchedule.appointmentId,
          title: futureSchedule.clientName
        })
      }
    }
    setScheduleData(dates);
  }, [barberFutureSchedule]);


  return (
    <div className="scheduler-wrapper">
      <div className='dor-check' ref={ref}>
        <Scheduler
          data={scheduleData}
          height={660}
        >
          <ViewState
            currentDate={filteredDate}
            onCurrentDateChange={setFilteredDate}
          />
          <WeekView
            startDayHour={9}
            endDayHour={19}
          />
          <Toolbar />
          <DateNavigator />
          <TodayButton />
          <Appointments />
          <AppointmentTooltip />
        </Scheduler>
      </div>
    </div>
  );
}
