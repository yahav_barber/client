import * as React from "react";
import { useContext, useEffect, useState } from "react";
import "../style.scss";
import "../style/index.scss";
import BackgroundVideo from "../assets/background.mp4";
import classNames from "classnames";
import { ApplicationProvider } from "../contexts/applicationContext";
import { AuthenticationProvider } from "../contexts/authenticationContext";
import Header from "../component/header/header";
import ApplicationBody from "../component/applicationBody/applicationBody";
import Gallery from "../component/gallery/gallery";
import { NotificationContainer } from 'react-notifications';


const IndexPage = () => {
  const [showForm, setShowForm] = useState(false);

  return (
    <ApplicationProvider>
      <AuthenticationProvider>
        <NotificationContainer />
        <main className={"page"}>
          <Header />
          <div className="background-container">
            <Gallery />
          </div>
          <ApplicationBody showForm={showForm} setShowForm={setShowForm} />
        </main>
      </AuthenticationProvider>
    </ApplicationProvider >
  );
};

export default IndexPage;
