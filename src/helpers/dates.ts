import { BarberAppointment } from "interfaces/barberAppointment";

export function getDatesOptionsBetweenTimeRanges(timeRanges: string[], appointmentMinutes?: number): Date[] {
    const options: Date[] = [];
    for (const timeRange of timeRanges) {
        const [startTimeDate, endTimeDate] = getStartAndEndTimeOfTimeRange(timeRange);

        let startFromTime = startTimeDate;

        while (startFromTime < endTimeDate) {
            options.push(new Date(startFromTime));
            if (appointmentMinutes) {
                startFromTime += appointmentMinutes * 1000 * 60;
            } else {
                startFromTime += endTimeDate - startTimeDate;
            }
        }
    }

    return options.sort((a, b) => a > b ? 1 : -1);;
}

export function isTimeRangeConflictWithBarberAppointments(timeRange: string, barberAppointments: BarberAppointment[] = []): boolean {
    const [startAvailableTime, endAvailableTime] = getStartAndEndTimeOfTimeRange(timeRange);

    for (const barberAppointment of barberAppointments) {
        const [startAppointmentTime, endAppointmentTime] = getStartAndEndTimeOfTimeRange(barberAppointment.timeRange);

        if ((startAppointmentTime > startAvailableTime && startAppointmentTime < endAvailableTime) ||
            (endAppointmentTime > startAvailableTime && endAppointmentTime < endAvailableTime)) {
            return true;
        }
    }

    return false;
}

export function isTimeRangeIntersectsWithOtherTimeRange(timeRanges: string[], intersectTimeRangeIndex: number): boolean {
    const [intersectStartTime, intersectEndTime] = getStartAndEndTimeOfTimeRange(timeRanges[intersectTimeRangeIndex]);

    if (intersectStartTime >= intersectEndTime) {
        return true;
    }

    for (let otherTimeRangeIndex = 0; otherTimeRangeIndex < timeRanges.length; otherTimeRangeIndex++) {
        if (intersectTimeRangeIndex !== otherTimeRangeIndex) {
            const [otherStartTime, otherEndTime] = getStartAndEndTimeOfTimeRange(timeRanges[otherTimeRangeIndex]);

            if ((otherStartTime > intersectStartTime && otherStartTime < intersectEndTime) ||
                (otherEndTime > intersectStartTime && otherEndTime < intersectEndTime)) {
                return true;
            }
        }

    }

    return false;
}

export function getFinishTimeOfAppointmentDate(appointmentDate: Date, appointmentMinutes: number) {
    const finishDate = new Date(appointmentDate.getTime() + appointmentMinutes * 1000 * 60)

    return formatDateToTime(finishDate);
}

export function formatDateToTime(date: Date) {
    const hours = formatTwoDigits(date.getHours());
    const minutes = formatTwoDigits(date.getMinutes());
    return hours + ":" + minutes;
}

export function convertTimeRangeInDateToDateObjects(date, timeRange: string): [Date, Date] {
    const [startTime, endTime] = timeRange.split("-");

    return [dateAndTimeToDateObject(date, startTime), dateAndTimeToDateObject(date, endTime)]
}

export function dateAndTimeToDateObject(date, time): Date {
    const [day, month, year] = date.split('-');
    const [hours, minutes] = time.split(':');
    const dateObject = new Date(`${month}/${day}/${year}`);

    dateObject.setHours(hours);
    dateObject.setMinutes(minutes);

    return dateObject;
}

function parseTimeToDateObject(time: string) {
    const [hours, minutes] = time.split(":");
    const d = new Date()
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), hours as any, minutes as any);
}

function formatTwoDigits(number: number) {
    return number < 10 ? '0' + number : number;
}

function getStartAndEndTimeOfTimeRange(timeRange: string) {
    const [startTime, endTime] = timeRange.split("-");

    const startAvailableTime = parseTimeToDateObject(startTime).getTime();
    const endAvailableTime = parseTimeToDateObject(endTime).getTime();

    return [startAvailableTime, endAvailableTime];
}