export interface Appointment {
    _id?: string;
    barberId: string;
    clientId: string;
    date: string;
    timeRange: string;
}
