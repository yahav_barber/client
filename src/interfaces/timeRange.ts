export interface TimeRange {
    timeRange: string;
    isPossible: boolean;
}