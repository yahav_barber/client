import { UserType } from "enums/userType";
import { Barber } from "./barber";
import { Client } from "./client";

export interface UserDetails {
    userType: UserType;
    user: Client | Barber;
}