export interface BarberSchedule {
    [date: string]: BarberAppointment[]
}

export interface BarberAppointment {
    appointmentId: string;
    clientId: string;
    clientName: string;
    timeRange: string;
}
