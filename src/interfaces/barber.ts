export interface Availability {
    [date: string]: string[]
}

export interface Barber {
    _id: string;
    name: string;
    phoneNumber: string;
    availability: Availability;
    appointmentMinutes: number;
}
